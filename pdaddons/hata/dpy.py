from hata import Client, ChannelTextBase, Message, CHANNELS

class KeepType(object):
    __slots__ = ('old_class')
    _ignored_attr_names = {'__name__', '__qualname__', '__weakref__', '__dict__', '__slots__'}

    def __new__(cls, old_class, *, new_class=None):
        self = object.__new__(cls)
        self.old_class = old_class
        if new_class is None:
            return self
        return self(new_class)

    def __call__(self, new_class):
        old_class = self.old_class
        ignored_attr_names = self._ignored_attr_names
        for name in dir(new_class):
            if name in ignored_attr_names:
                continue
            attr = getattr(new_class, name)
            if hasattr(object, name) and (attr is getattr(object, name)):
                continue
            setattr(old_class, name, attr)
        return old_class

@KeepType(ChannelTextBase)
class MChannelTextBase:
    async def send(self, *args, **kwargs):
        clients = self.clients
        if not clients:
            raise RuntimeError("This channel does not have any clients")
        return (await clients[0].message_create(self, *args, **kwargs))

@KeepType(Message)
class MMessage:
    async def send(self, *args, **kwargs):
        await self.channel.send(*args, **kwargs)

@KeepType(Client)
class MClient:
    async def get_channel(*args, **kwargs):
        return CHANNELS.get(*args, **kwargs)
