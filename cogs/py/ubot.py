@pdbot.commands
async def ubot(client, message):
    if not message.author.has_role(ROLES.get(int(os.getenv('PDBOT_TRUSTED')))):
       await message.send(embed=Embed(title="Insufficient Permission!", description="Missing Role ``PDBot Trusted``", color=Color.from_rgb(255, 215, 0)))
       return
    await message.send(embed=Embed(title="Reloading Bot", description="```Updating...```",color=Color.from_rgb(255, 215, 0)))
    async with enter_executor():
        os.system("./stop.sh")
