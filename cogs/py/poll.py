@client.command(description='Creates a poll||<title(in double quotes)> <question(in double quotes)> <emojis(example emoji: example answer)>')
async def poll(ctx, title: str, question: str, *, emojis):
    if ctx.channel.id == 668006657791492098 or ctx.channel.id == 668013799646953486 or ctx.channel.id == 680956912048537640:
        poll_channel = client.get_channel(748514616966905866)
        emojis = emojis.split("\n")
        emojis_dict = []
        for i in emojis:
            for j in i.split(':'):
                emojis_dict.append(j)
        emoji_list = []
        poll_desc = ''

        for i in range(len(emojis_dict)):
            if i % 2 == 0: # emojis
                emoji_list.append(str(emojis_dict[i]))
                poll_desc = f'{poll_desc}{emojis_dict[i].strip()}:'
            else: # questions
                poll_desc = f'{poll_desc} {emojis_dict[i].strip()}\n'

        poll_role = ctx.guild.get_role(718829130308649032)
        embed = discord.Embed(title=title, color=discord.Color.from_rgb(randint(0,255), randint(0,255), randint(0,255)),
                                description=f'{question}\n\n```{poll_desc}```')
        embed.set_thumbnail(url=ctx.message.author.avatar_url)
        embed.set_footer(text=f'Poll started by: {ctx.message.author}', icon_url=ctx.message.author.avatar_url)

        canReact: bool = True
        try:
            for i in emoji_list:
                await ctx.message.add_reaction(i.strip())
        except:
            canReact = False
            await poll.delete()
            embed = discord.Embed(title='Error! ⚠️', color=discord.Color.from_rgb(255, 255, 51),
                                    description='The emojis were entered incorrectly!')
            embed.set_footer(text=f'Attempted by: {ctx.message.author}', icon_url=ctx.author.avatar_url)
            await ctx.channel.send(embed=embed)
        if canReact:
            try:
                poll = await poll_channel.send(content=f'||{poll_role.mention}||', embed=embed)
                for i in emoji_list:
                    await poll.add_reaction(await ctx.guild.fetch_emoji(i.strip()))
            except:
                embed = discord.Embed(title='Error! ⚠️', color=discord.Color.from_rgb(255, 255, 51),
                                        description='The message is too long!')
                embed.set_footer(text=f'Attempted by: {ctx.message.author}', icon_url=ctx.author.avatar_url)
                await ctx.channel.send(embed=embed)

    else:
        embed = discord.Embed(title='Error! ⚠️', color=discord.Color.from_rgb(255, 255, 51),
                                description=f'Please use this command in {bot_channel.mention}!')
        embed.set_footer(text=f'Attempted by: {ctx.message.author}', icon_url=ctx.author.avatar_url)
        await ctx.channel.send(embed=embed)
