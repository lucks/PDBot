@client.command(description='Removes channels from a project||<project-name> <channel1> <channel2> <...>')
async def dc(ctx, projectname: str, *, channels='general'):
    projectname = projectname.lower()
    if get(ctx.guild.roles, name=f'{projectname} Founder'):
        if get(ctx.author.roles, name=f"{projectname} Founder"):
            category = get(ctx.guild.categories, name=projectname)
            channels = [x.lower() for x in channels.split()]
            for i in channels:
                try:
                    channel = get(ctx.guild.channels, name=i, category=category)
                    await channel.delete()
                    await asyncio.sleep(1)
                except:
                    pass
            embed = discord.Embed(color=discord.Color.from_rgb(0, 255, 0), description=f'Channels removed from **{projectname}**!')
            embed.set_footer(text=f'Channels removed by: {ctx.message.author}', icon_url=ctx.author.avatar_url)
            await ctx.channel.send(embed=embed)
        else:
            embed = discord.Embed(title='Error! ⚠️', color=discord.Color.from_rgb(255, 255, 51),
                                    description='You must be the founder of the project to add channels!')
            embed.set_footer(text=f'Attempted by: {ctx.message.author}', icon_url=ctx.author.avatar_url)
            await ctx.channel.send(embed=embed)

    else:
        embed = discord.Embed(title='Error! ⚠️', color=discord.Color.from_rgb(255, 255, 51),
                                description='That project does not exist!')
        embed.set_footer(text=f'Attempted by: {ctx.message.author}', icon_url=ctx.author.avatar_url)
        await ctx.channel.send(embed=embed)
