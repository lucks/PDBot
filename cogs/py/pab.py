def bot_check(bot_id):
    bots = [302050872383242240, 694698809464324226, 422087909634736160, 212681528730189824, 115385224119975941, 341738423134060544, 159985870458322944, 673395088071589898, 512333785338216465]
    for i in bots:
        if i == bot_id:
            return True
    return False

@client.command(description='Adds a bot to your project||<project-name> <bot-name>')
async def pab(ctx, projectname: str, bot: discord.Member):
    projectname = projectname.lower()
    if get(ctx.guild.roles, name=f'{projectname} Founder'):
        if get(ctx.author.roles, name=f"{projectname} Founder"):
            if bot:
                if bot.bot:
                    if not bot_check(bot.id):
                        channel_overwrites = discord.PermissionOverwrite(create_instant_invite = False,
                                                                        manage_channels = True,
                                                                        manage_permissions = False,
                                                                        manage_webhooks = True,
                                                                        view_channel = True,
                                                                        send_messages = True,
                                                                        send_tts_messages = False,
                                                                        manage_messages = True,
                                                                        embed_links = True,
                                                                        attach_files = True,
                                                                        read_message_history = True,
                                                                        mention_everyone = False,
                                                                        use_external_emojis = True,
                                                                        add_reactions = True,
                                                                        connect = True,
                                                                        speak = True,
                                                                        stream = False,
                                                                        mute_members = False,
                                                                        deafen_members = False,
                                                                        move_members = False,
                                                                        use_voice_activation = True,
                                                                        priority_speaker = False)

                        channels = get(ctx.guild.categories, name=projectname).channels
                        for channel in channels:
                            await channel.set_permissions(bot, overwrite=channel_overwrites)
                        embed = discord.Embed(color=discord.Color.from_rgb(0, 255, 0), description=f'{bot.mention} added to **{projectname}**!')
                        embed.set_footer(text=f'Bot added to project by: {ctx.message.author}', icon_url=ctx.author.avatar_url)
                        await ctx.channel.send(embed=embed)
                    else:
                        embed = discord.Embed(title='Error! ⚠️', color=discord.Color.from_rgb(255, 255, 51),
                                                description='You may not add this bot to your project!')
                        embed.set_footer(text=f'Attempted by: {ctx.message.author}', icon_url=ctx.author.avatar_url)
                        await ctx.channel.send(embed=embed)
                else:
                    embed = discord.Embed(title='Error! ⚠️', color=discord.Color.from_rgb(255, 255, 51),
                                            description='You may only add bots to your project!')
                    embed.set_footer(text=f'Attempted by: {ctx.message.author}', icon_url=ctx.author.avatar_url)
                    await ctx.channel.send(embed=embed)
            else:
                embed = discord.Embed(title='Error! ⚠️', color=discord.Color.from_rgb(255, 255, 51),
                                        description='That bot does not exist!')
                embed.set_footer(text=f'Attempted by: {ctx.message.author}', icon_url=ctx.author.avatar_url)
                await ctx.channel.send(embed=embed)
        else:
            embed = discord.Embed(title='Error! ⚠️', color=discord.Color.from_rgb(255, 255, 51),
                                    description='You must be the founder of the project to add channels!')
            embed.set_footer(text=f'Attempted by: {ctx.message.author}', icon_url=ctx.author.avatar_url)
            await ctx.channel.send(embed=embed)
    else:
        embed = discord.Embed(title='Error! ⚠️', color=discord.Color.from_rgb(255, 255, 51),
                                description='That project does not exist!')
        embed.set_footer(text=f'Attempted by: {ctx.message.author}', icon_url=ctx.author.avatar_url)
        await ctx.channel.send(embed=embed)
