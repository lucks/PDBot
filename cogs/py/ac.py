@client.command(description='Adds channels to a project||<project-name> <channel1> <channel2> <...>')
async def ac(ctx, projectname, *, channels='general'):
    projectname = projectname.lower()
    if get(ctx.guild.roles, name=f'{projectname} Founder'):
        if get(ctx.author.roles, name=f"{projectname} Founder"):
            founder = get(ctx.guild.roles, name=f'{projectname} Founder')
            dev = get(ctx.guild.roles, name=f'{projectname} Dev')
            manager = get(ctx.guild.roles, name='Manager')
            mod = get(ctx.guild.roles, name='Moderator')
            bot = get(ctx.guild.roles, name='PDBot')

            channel_overwrites = {
                ctx.guild.default_role: discord.PermissionOverwrite(create_instant_invite = False,
                                                manage_channels = False,
                                                manage_permissions = False,
                                                manage_webhooks = False,
                                                view_channel = False,
                                                send_messages = False,
                                                send_tts_messages = False,
                                                manage_messages = False,
                                                embed_links = False,
                                                attach_files = False,
                                                read_message_history = False,
                                                mention_everyone = False,
                                                use_external_emojis = False,
                                                add_reactions = False,
                                                connect = False,
                                                speak = False,
                                                stream = False,
                                                mute_members = False,
                                                deafen_members = False,
                                                move_members = False,
                                                use_voice_activation = False,
                                                priority_speaker = False),
                founder: discord.PermissionOverwrite(create_instant_invite = False,
                                                manage_channels = True,
                                                manage_permissions = False,
                                                manage_webhooks = True,
                                                view_channel = True,
                                                send_messages = True,
                                                send_tts_messages = False,
                                                manage_messages = True,
                                                embed_links = True,
                                                attach_files = True,
                                                read_message_history = True,
                                                mention_everyone = False,
                                                use_external_emojis = True,
                                                add_reactions = True,
                                                connect = True,
                                                speak = True,
                                                stream = True,
                                                mute_members = False,
                                                deafen_members = False,
                                                move_members = False,
                                                use_voice_activation = True,
                                                priority_speaker = False),
                dev: discord.PermissionOverwrite(create_instant_invite = False,
                                                manage_channels = False,
                                                manage_permissions = False,
                                                manage_webhooks = False,
                                                view_channel = True,
                                                send_messages = True,
                                                send_tts_messages = False,
                                                manage_messages = False,
                                                embed_links = True,
                                                attach_files = True,
                                                read_message_history = True,
                                                mention_everyone = False,
                                                use_external_emojis = True,
                                                add_reactions = True,
                                                connect = True,
                                                speak = True,
                                                stream = True,
                                                mute_members = False,
                                                deafen_members = False,
                                                move_members = False,
                                                use_voice_activation = True,
                                                priority_speaker = False),
                manager: discord.PermissionOverwrite(create_instant_invite = False,
                                                manage_channels = True,
                                                manage_permissions = False,
                                                manage_webhooks = True,
                                                view_channel = True,
                                                send_messages = True,
                                                send_tts_messages = False,
                                                manage_messages = True,
                                                embed_links = True,
                                                attach_files = True,
                                                read_message_history = True,
                                                mention_everyone = False,
                                                use_external_emojis = True,
                                                add_reactions = True,
                                                connect = True,
                                                speak = True,
                                                stream = True,
                                                mute_members = True,
                                                deafen_members = False,
                                                move_members = True,
                                                use_voice_activation = True,
                                                priority_speaker = False),
                mod: discord.PermissionOverwrite(create_instant_invite = False,
                                                manage_channels = False,
                                                manage_permissions = False,
                                                manage_webhooks = False,
                                                view_channel = True,
                                                send_messages = True,
                                                send_tts_messages = False,
                                                manage_messages = True,
                                                embed_links = True,
                                                attach_files = True,
                                                read_message_history = True,
                                                mention_everyone = False,
                                                use_external_emojis = True,
                                                add_reactions = True,
                                                connect = True,
                                                speak = True,
                                                stream = True,
                                                mute_members = False,
                                                deafen_members = False,
                                                move_members = True,
                                                use_voice_activation = True,
                                                priority_speaker = False),
                bot: discord.PermissionOverwrite(create_instant_invite = False,
                                                manage_channels = True,
                                                manage_permissions = True,
                                                manage_webhooks = True,
                                                view_channel = True,
                                                send_messages = True,
                                                send_tts_messages = False,
                                                manage_messages = True,
                                                embed_links = True,
                                                attach_files = True,
                                                read_message_history = True,
                                                mention_everyone = False,
                                                use_external_emojis = False,
                                                add_reactions = False,
                                                connect = True,
                                                speak = False,
                                                stream = False,
                                                mute_members = False,
                                                deafen_members = False,
                                                move_members = False,
                                                use_voice_activation = False,
                                                priority_speaker = False)
            }

            category = get(ctx.guild.categories, name=projectname)
            channels = [x.lower() for x in channels.split()]

            for i in channels:
                await ctx.guild.create_text_channel(i, category=category, overwrites=channel_overwrites)
            embed = discord.Embed(color=discord.Color.from_rgb(0, 255, 0), description=f'Channels added to **{projectname}**!')
            embed.set_footer(text=f'Channels added by: {ctx.message.author}', icon_url=ctx.author.avatar_url)
            await ctx.channel.send(embed=embed)
        else:
            embed = discord.Embed(title='Error! ⚠️', color=discord.Color.from_rgb(255, 255, 51),
                                    description='You must be the founder of the project to add channels!')
            embed.set_footer(text=f'Attempted by: {ctx.message.author}', icon_url=ctx.author.avatar_url)
            await ctx.channel.send(embed=embed)

    else:
        embed = discord.Embed(title='Error! ⚠️', color=discord.Color.from_rgb(255, 255, 51),
                                description='That project does not exist!')
        embed.set_footer(text=f'Attempted by: {ctx.message.author}', icon_url=ctx.author.avatar_url)
        await ctx.channel.send(embed=embed)
