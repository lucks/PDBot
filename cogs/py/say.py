@client.command(description='Sends an embed||<title> <message>')
@commands.has_any_role('Owner', 'Manager', 'Moderator')
async def say(ctx, title, *, message):
    await ctx.message.delete()
    embed = discord.Embed(title=title, color=discord.Color.from_rgb(0, 191, 255),
                        description=message)
    embed.set_footer(text=f'By: {ctx.message.author}', icon_url=ctx.message.author.avatar_url)
    await ctx.send(embed=embed)
