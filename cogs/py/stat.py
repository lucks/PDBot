import datetime
from datetime import date
@client.command(description='Shows user info||<member>')
async def stat(ctx, member: discord.Member=None):
    member = member or ctx.author
    # fvalue = date.today() - member.joined_at.date()
    if member.bot == True:
        fbot = "Yes"
    else:
        fbot = "No"

    embed = discord.Embed(title=f"{member.name} Info", colour=discord.Color.from_rgb(255, 215, 0), icon_url=member.avatar_url)
    embed.add_field(name="Name:", value=member)
    embed.add_field(name="User ID:", value=member.id)
    embed.add_field(name="In guild name:", value=member.nick, inline=True)
    embed.add_field(name="Joined at:", value=member.joined_at.strftime("%d %b %Y"), inline=True)
    embed.add_field(name="Created at:", value=member.created_at.strftime("%#d %b %Y"), inline=True)
    embed.add_field(name="Bot: ", value=f"{fbot}", inline=True)
    embed.set_thumbnail(url=member.avatar_url)
    embed.set_footer(text=f"Requested by: {ctx.author.name}", icon_url=ctx.author.avatar_url)

    await ctx.send(embed=embed)
