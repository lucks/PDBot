from subprocess import getoutput
from hata import enter_executor, ROLES

@pdbot.commands
async def sys(client, msg, cmd):
    if msg.author.has_role(ROLES.get(int(os.environ.get("PDBOT_TRUSTED")))):
        await msg.channel.send("```\n"+getoutput(cmd)+"```")
