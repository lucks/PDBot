@client.command(description='Deletes a message in a channel||<message-id>')
async def rm(ctx, msgId: int):
    if not ctx.author.permissions_in(ctx.channel).manage_messages:
        embed = discord.Embed(title='Error! ⚠️', color=discord.Color.from_rgb(255, 255, 51), description='You do not have the correct permissions (manage messages) to use this command!')
        embed.set_footer(text=f'Attempted By: {ctx.message.author}', icon_url=ctx.author.avatar_url)
        await ctx.send(embed=embed)
        return

    embed = discord.Embed(color=discord.Color.from_rgb(0, 255, 0), description=f'Message deleted!')
    embed.set_footer(text=f'Messages deleted by: {ctx.message.author}', icon_url=ctx.author.avatar_url)
    await ctx.message.delete()
    msg = await ctx.fetch_message(msgId)
    await msg.delete()
    await ctx.send(embed=embed, delete_after=10)

    # amount = int(amount)
    # embed = discord.Embed(color=discord.Color.from_rgb(0, 255, 0), description=f'Deleted {amount} messages!')
    # embed.set_footer(text=f'Messages deleted by: {ctx.message.author}', icon_url=ctx.author.avatar_url)
    # await ctx.channel.purge(limit=(amount + 1))
    # await ctx.send(embed=embed, delete_after=10)
