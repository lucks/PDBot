@client.command(description='Deletes project||<project-name>')
async def dp(ctx, projectname: str):
    bot_channel = client.get_channel(668006657791492098)
    if ctx.channel == bot_channel or ctx.channel.id == 668013799646953486 or ctx.channel.id == 680956912048537640:
        projectname = projectname.lower()
        if get(ctx.author.roles, name=f"{projectname} Founder"):
            embed = discord.Embed(color=discord.Color.from_rgb(178, 34, 34), description=f'Deleted **{projectname}**!')
            embed.set_footer(text=f'Project deleted by: {ctx.message.author}', icon_url=ctx.author.avatar_url)
            await ctx.channel.send(embed=embed)

            founder = get(ctx.guild.roles, name=f"{projectname} Founder")
            dev = get(ctx.guild.roles, name=f"{projectname} Dev")
            category = get(ctx.guild.categories, name=projectname)

            await founder.delete()
            await dev.delete()

            while True:
                try:
                    channel = get(ctx.guild.channels, category=category)
                    await channel.delete()
                    await asyncio.sleep(1)
                except:
                    break
            await category.delete()
        else:
            embed = discord.Embed(title='Error! ⚠️', color=discord.Color.from_rgb(255, 255, 51),
                                    description='You must be the founder of the project to delete it!')
            embed.set_footer(text=f'Attempted by: {ctx.message.author}', icon_url=ctx.author.avatar_url)
            await ctx.channel.send(embed=embed)
    else:
        embed = discord.Embed(title='Error! ⚠️', color=discord.Color.from_rgb(255, 255, 51),
                                description=f'Please use this command in {bot_channel.mention}!')
        embed.set_footer(text=f'Attempted by: {ctx.message.author}', icon_url=ctx.author.avatar_url)
        await ctx.channel.send(embed=embed)
