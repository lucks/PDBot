#@client.listen()
#async def on_message_delete(message):
#    log_channel = client.get_channel(668009901884309515)
#    if not message.author.id == 383995098754711555:
#        if message.channel.id != 713233286574637127: # counter channel
#            try:
#                embed = discord.Embed(title='Message Deleted 🗑️', color=discord.Color.from_rgb(178, 34, 34))
#                embed.add_field(name='Content:', value=message.content)
#                embed.add_field(name='Channel:', value=message.channel.mention)
#                embed.set_footer(text=f'Deleted message author: {message.author}', icon_url=message.author.avatar_url)
#                await log_channel.send(embed=embed)
#            except:
#                embed = discord.Embed(title='Message Deleted 🗑️', color=discord.Color.from_rgb(178, 34, 34),
#                                        description='Unable to recover')
#                await log_channel.send(embed=embed)

@client.listen()
async def on_message_edit(old_message, new_message):
    log_channel = client.get_channel(668009901884309515)
    if not old_message.pinned and new_message.pinned:
        embed = discord.Embed(title='Message Pinned 📌', description=new_message.content, color=discord.Color.from_rgb(0, 255, 0))
        embed.add_field(name='Channel:', value=new_message.channel.mention)
        embed.add_field(name='Message Author:', value=new_message.author.mention)
        embed.set_thumbnail(url=new_message.author.avatar_url)
        await log_channel.send(embed=embed)
    elif old_message.pinned and not new_message.pinned:
        embed = discord.Embed(title='Message Unpinned 📌', description=new_message.content, color=discord.Color.from_rgb(255, 0, 0))
        embed.add_field(name='Channel:', value=new_message.channel.mention)
        embed.add_field(name='Message Author:', value=new_message.author.mention)
        embed.set_thumbnail(url=new_message.author.avatar_url)
        await log_channel.send(embed=embed)
    elif old_message.content.split() != new_message.content.split():
        if 'discord.gg/' in new_message.content.lower() or 'discordapp.com/invite' in new_message.content.lower():
            if new_message.author.id != 300126997718237195:
                await new_message.delete()
        if not new_message.author.id == 383995098754711555:
#            try:
#                embed = discord.Embed(title='Message Updated ♻️', color=discord.Color.from_rgb(178, 34, 34))
#                embed.add_field(name='Old:', value=old_message.content)
#                embed.add_field(name='New:', value=new_message.content)
#                embed.add_field(name='Channel:', value=old_message.channel.mention)
#                embed.set_footer(text=f'Message updated by: {old_message.author}', icon_url=old_message.author.avatar_url)
#                await log_channel.send(embed=embed)
#            except:
#                embed = discord.Embed(title='Message Updated ♻️', color=discord.Color.from_rgb(178, 34, 34),
#                                        description='Failed to display contents')
#                await log_channel.send(embed=embed)
            if new_message.channel.id == 713233286574637127: # counter channel
                if new_message == new_message.channel.last_message:
                    await new_message.delete()

@client.listen()
async def on_message(message):
    if message.channel.id == 709783674857586759: # verify channel
        if not message.author.id == 300126997718237195:
            await message.delete(delay=60)
    elif message.channel.id == 713233286574637127: # counter channel
        unallowed = ['+', '-', '*', '/', '%', '**', '//']
        for i in unallowed:
            if i in message.content:
                await message.delete()
                return
        try:
            next_num = int(message.content)
            previous_num = int((await message.channel.history(limit=2).flatten())[1].content)
            if next_num == (previous_num + 1):
                if next_num % 1000 == 0:
                    await message.pin()
            else:
                await message.delete()
        except:
            await message.delete()
    # if 'discord.gg/' in message.content.lower() or 'discordapp.com/invite' in message.content.lower():
    #     if message.author.id != 300126997718237195:
    #         await message.delete()

@client.listen()
async def on_reaction_add(reaction, user):
    if not user.bot:
        if not reaction.message.author.id == 383995098754711555:
            try:
                log_channel = client.get_channel(668009901884309515)
                embed = discord.Embed(title=f'Reaction added {reaction.emoji}', color=discord.Color.from_rgb(0, 255, 0))
                embed.add_field(name='Message:', value=reaction.message.content)
                embed.add_field(name='Channel:', value=reaction.message.channel.mention)
                embed.set_footer(text=f'Reaction added by: {user}', icon_url=user.avatar_url)
                await log_channel.send(embed=embed)
            except:
                pass
        if reaction.emoji == '📌' and reaction.count == 3:
            await reaction.message.pin()

@client.listen()
async def on_reaction_remove(reaction, user):
    if not user.bot:
        if not reaction.message.author.id == 383995098754711555:
            try:
                log_channel = client.get_channel(668009901884309515)
                embed = discord.Embed(title=f'Reaction removed {reaction.emoji}', color=discord.Color.from_rgb(178, 34, 34))
                embed.add_field(name='Message:', value=reaction.message.content)
                embed.add_field(name='Channel:', value=reaction.message.channel.mention)
                embed.set_footer(text=f'Reaction removed by: {user}', icon_url=user.avatar_url)
                await log_channel.send(embed=embed)
            except:
                pass

@client.listen()
async def on_member_join(member):
    if not member.bot:
        log_channel = client.get_channel(668009901884309515)
        welcome_channel = client.get_channel(668005380630118410)
        info_channel = client.get_channel(668011780479647764)
        role_channel = client.get_channel(668016959836913674)
        embed1 = discord.Embed(title=f'New user!', description=f'**{member.name}** has joined the server', color=discord.Color.from_rgb(0, 255, 0))
        embed1.add_field(name="User ID - ", value=member.id, inline=False)
        embed1.add_field(name="Created At - ", value=member.created_at.strftime("%#d %b %Y, %I:%M:%S %p"), inline=False)
        embed1.set_thumbnail(url=member.avatar_url)
        await log_channel.send(embed=embed1)
        embed = discord.Embed(title=f'[**{member.name}**]', description=f"Welcome to __Programmer's Den__ **{member.name}**! Head {info_channel.mention} to get started and head to {role_channel.mention} to choose your languages!", color=discord.Color.from_rgb(0, 255, 0))
        embed.set_thumbnail(url=member.avatar_url)
        embed.set_footer(text=f'ID: {member.id}')
        await welcome_channel.send(embed=embed)
    else:
        log_channel = client.get_channel(668009901884309515)
        embed = discord.Embed(title=f'New bot!', description=f'**{member.mention}** has been added!', color=discord.Color.from_rgb(0, 255, 0))
        embed.set_thumbnail(url=member.avatar_url)
        await log_channel.send(embed=embed)
        bot_role = member.guild.get_role(668008527004041226)
        await member.add_roles(bot_role)

@client.listen()
async def on_member_remove(member):
    log_channel = client.get_channel(668009901884309515)
    embed = discord.Embed(title='User left', description=f'**{member.name}** has left the server', color=discord.Color.from_rgb(255, 0, 0))
    embed.add_field(name="Joined Date", value=member.joined_at.strftime("%#d %b %Y, %I:%M:%S %p"), inline=False)
    embed.set_thumbnail(url=member.avatar_url)
    await log_channel.send(embed=embed)

# @client.listen()
# async def on_member_ban(guild, user):
#     log_channel = client.get_channel(668009901884309515)
#     embed = discord.Embed(title=f'Banned {user} 🔒', description=f'**{user.name}** has been banned!', color=discord.Color.from_rgb(255, 0, 0))
#     embed.set_thumbnail(url=user.avatar_url)
#     await log_channel.send(embed=embed)

# @client.listen()
# async def on_member_unban(guild, user):
#     log_channel = client.get_channel(668009901884309515)
#     embed = discord.Embed(title=f'Unbanned {user} 🔓', description=f'**{user.name}** has been unbanned!', color=discord.Color.from_rgb(0, 255, 0))
#     embed.set_thumbnail(url=user.avatar_url)
#     await log_channel.send(embed=embed)

# @client.listen()
# async def on_member_update(before, after):
#     log_channel = client.get_channel(668009901884309515)
#     if before.nick != after.nick:
#         embed = discord.Embed(title=f'Nickname updated!', color=discord.Color.from_rgb(0, 191, 255))
#         embed.add_field(name='Before:', value=before.nick)
#         embed.add_field(name='After:', value=after.nick)
#         embed.set_thumbnail(url=after.avatar_url)
#         await log_channel.send(embed=embed)
    # if before.roles != after.roles:
    #     muted_role = after.guild.get_role(682105242199916757)
    #     member_role = after.guild.get_role(709780584276295761)
    #     roles = list(set(before.roles) - set(after.roles))
    #     if roles: # when role is removed
    #         for i in roles:
    #             if i == muted_role:
    #                 await after.add_roles(member_role)
                # embed = discord.Embed(title=f'Role removed!', description=f'**{after.name}** has removed {i.name}', color=discord.Color.from_rgb(255, 0, 0))
        # else: # when role is added
        #     roles = list(set(after.roles) - set(before.roles))
        #     for i in roles:
        #         if i == muted_role:
        #             await after.remove_roles(member_role)
                # embed = discord.Embed(title=f'Role added!', description=f'**{after.name}** has added {i.name}', color=discord.Color.from_rgb(0, 255, 0))
        # embed.set_thumbnail(url=after.avatar_url)
        # await log_channel.send(embed=embed)

# @client.listen()
# async def on_user_update(before, after):
#     if before.avatar_url != after.avatar_url:
#         log_channel = client.get_channel(668009901884309515)
#         embed = discord.Embed(title=f'Profile picture updated!', color=discord.Color.from_rgb(0, 191, 255))
#         old = before.avatar_url
#         new = after.avatar_url
#         embed.add_field(name='Before:', value=old)
#         embed.add_field(name='After:', value=new)
#         embed.set_thumbnail(url=before.avatar_url)
#         embed.set_footer(text=f'By: {after}', icon_url=after.avatar_url)
#         await log_channel.send(embed=embed)

# @client.listen()
# async def on_guild_channel_create(channel):
#     log_channel = client.get_channel(668009901884309515)
#     embed = discord.Embed(title='Channel created! ✏️', description=f'{channel.name} has been created!', color=discord.Color.from_rgb(0, 255, 0))
#     embed.add_field(name='Category:', value=channel.category.name)
#     embed.add_field(name='Channel:', value=channel.mention)
#     await log_channel.send(embed=embed)

# @client.listen()
# async def on_guild_channel_delete(channel):
#     log_channel = client.get_channel(668009901884309515)
#     embed = discord.Embed(title='Channel deleted! 🗑️', description=f'{channel.name} has been deleted!', color=discord.Color.from_rgb(255, 0, 0))
#     embed.add_field(name='Category:', value=channel.category.name)
#     embed.add_field(name='Channel:', value=channel.mention)
#     await log_channel.send(embed=embed)

# @client.listen()
# async def on_guild_channel_update(before, after):
#     log_channel = client.get_channel(668009901884309515)
#     if after.category_id != 699385335398006855:
#         if before.name != after.name:
#             embed = discord.Embed(title=f'Channel updated! 📄', description=f'{after.name}', color=discord.Color.from_rgb(0, 191, 255))
#             embed.add_field(name='Before:', value=before.name)
#             embed.add_field(name='After:', value=after.name)
#             embed.add_field(name='Channel:', value=after.mention)
#             await log_channel.send(embed=embed)
#         elif before.overwrites != after.overwrites:
#             before_perms = list(before.overwrites)
#             after_perms = list(after.overwrites)
#             embed = discord.Embed(title=f'Permissions updated! 📄', color=discord.Color.from_rgb(0, 191, 255))
#             for i in before_perms:
#                 before_perms.iter()
#             for i in after_perms:
#                 if i == discord.PermissionOverwrite():
#                     embed.add_field(name='After perms:', value=i.pair())
#             embed.add_field(name='Channel:', value=before.mention)
#             await log_channel.send(embed=embed)

# @client.listen()
# async def on_guild_emojis_update(guild, before, after):
#     log_channel = client.get_channel(668009901884309515)
#     if before != after:
#         embed = discord.Embed(title='Emoji updated!', color=discord.Color.from_rgb(0, 191, 255))
#         if before:
#             changes = list(set(before) - set(after))
#         else:
#             changes = before
#         for i in changes:
#             embed.add_field(name='Changes:', value=i.name)
#         await log_channel.send(embed=embed)
