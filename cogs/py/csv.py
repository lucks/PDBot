@client.command(description='Creates a staff vote||<timer(mins)> <desc>')
@commands.has_any_role('Owner', 'Manager', 'Moderator')
async def csv(ctx, timer: float, *, desc: str):
    global staff_vote_amount
    vote_channel = client.get_channel(691901559247863888)
    if 'staff_vote_amount' in globals():
        pass
    else:
        staff_vote_amount = 0
    current_vote_count = staff_vote_amount + 1
    embed = discord.Embed(title=f'Vote #{current_vote_count} [{timer} mins]', color=discord.Color.from_rgb(0, 255, 0),
                            description=f'{desc}\n\n```✅: Yes\n🚫: No```')
    embed.set_footer(text=f'Vote started by: {ctx.message.author}', icon_url=ctx.author.avatar_url)
    vote = await vote_channel.send(content='||@here||', embed=embed)

    staff_vote_amount += 1

    for i in ['✅', '🚫']:
        await vote.add_reaction(i)
    await asyncio.sleep(float(timer) * 60)

    vote = await vote_channel.fetch_message(vote.id)
    for i in vote.reactions:
        if i.emoji == '✅':
            yes = get(vote.reactions, emoji=i.emoji).count
        elif i.emoji == '🚫':
            no = get(vote.reactions, emoji=i.emoji).count
        else:
            pass
    if yes > no:
        embed = discord.Embed(title=f'Vote #{current_vote_count} ✅', color=discord.Color.from_rgb(0, 255, 0),
                                description=f'Approved!\n\n`✅ had {yes - no} more vote(s)!`\n\n```✅: {yes}\n🚫: {no}```')
    elif no > yes:
        embed = discord.Embed(title=f'Vote #{current_vote_count} 🚫', color=discord.Color.from_rgb(178, 34, 34),
                                description=f'Denied!\n\n`🚫 had {no - yes} more vote(s)!`\n\n```✅: {yes}\n🚫: {no}```')
    else:
        embed = discord.Embed(title=f'Vote #{current_vote_count} ❗', color=discord.Color.from_rgb(0, 191, 255),
                                description=f'Tied!\n\n`Vote tied!`\n\n```✅: {yes}\n🚫: {no}```')

    embed.set_footer(text=f'Ended the vote made by: {ctx.message.author}', icon_url=ctx.author.avatar_url)
    await vote_channel.send(embed=embed)
    staff_vote_amount -= 1
