@client.command(description='Creates announcement||<general/add/remove/modify> <title> <desc>')
@commands.has_any_role('Owner', 'Manager', 'Moderator')
async def ca(ctx, option: str, title: str, message: str):
    if option == 'general' or option == 'add' or option == 'remove' or option == 'modify':
        if option == 'general':
            embed = discord.Embed(title=f':tada: {title}', color=discord.Color.from_rgb(0, 191, 255), description=message)
        elif option == 'add':
            embed = discord.Embed(title=f'[{title}] ✅', color=discord.Color.from_rgb(0, 255, 0), description=message)
        elif option == 'remove':
            embed = discord.Embed(title=f'[{title}] 🚫', color=discord.Color.from_rgb(178, 34, 34), description=message)
        elif option == 'modify':
            embed = discord.Embed(title=f'[{title}] 🔧', color=discord.Color.from_rgb(255, 255, 51), description=message)
        announce = client.get_channel(668290603779489792)
        annoucement_role = ctx.guild.get_role(709843164508323931)
        embed.set_footer(text=f'Author: {ctx.message.author}', icon_url=ctx.author.avatar_url)
        await announce.send(content=f'||{annoucement_role.mention}||', embed=embed)
        embed = discord.Embed(color=discord.Color.from_rgb(0, 255, 0), description='Announcement posted!')
        embed.set_footer(text=f'Announcement posted by: {ctx.message.author}', icon_url=ctx.author.avatar_url)
        await ctx.channel.send(embed=embed)
    else:
        embed = discord.Embed(title='Error! ⚠️', color=discord.Color.from_rgb(255, 255, 51), description='Invalid option!')
        embed.set_footer(text=f'Attempted by: {ctx.message.author}', icon_url=ctx.author.avatar_url)
        await ctx.channel.send(embed=embed)
