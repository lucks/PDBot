import requests
from bs4 import BeautifulSoup

@client.command(description='Displays ascii fonts||None')
async def artfonts(ctx):
    try:
        page = requests.get('http://www.figlet.org/fontdb.cgi')
        soup = BeautifulSoup(page.content, 'html.parser')
        fonts = soup.find_all('td')
        font = ''

        for i in fonts[1::3]:
            text = i.get_text().strip()
            if text != 'Font Name' and text != 'Contact us at info@figlet.org':
                font = f'{font}{text}\n'

        embed = discord.Embed(title='Fonts', color=discord.Color.from_rgb(0, 191, 255), 
                                description=font)
        embed.set_footer(text=f'Requested by: {ctx.message.author}', icon_url=ctx.author.avatar_url)
        await ctx.channel.send(embed=embed)
    except Exception as e:
        embed = discord.Embed(title='Error! ⚠️', color=discord.Color.from_rgb(255, 255, 51),
                                description=f'Failed to get fonts due to: {e}')
        embed.set_footer(text=f'Attempted by: {ctx.message.author}', icon_url=ctx.author.avatar_url)
        await ctx.channel.send(embed=embed)
