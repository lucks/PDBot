# Adapted from Yuudachi from iCrawl
import requests
DJS_DOCS_URI='https://djsdocs.sorta.moe/v2/embed?'
DOCUMENTATION_SOURCES=('stable', 'master', 'rpc', 'commando', 'akairo', 'akairo-master', 'v11', 'collection')

@client.command(description='Fetches Discord.JS Documentation||<query> [source]')
async def djs(ctx, query: str, *, source='stable'):
    if source not in DOCUMENTATION_SOURCES:
        embed = discord.Embed(title='Error! ⚠️', color=discord.Color.from_rgb(255, 255, 51),
                                description='Unknown source!')
        embed.set_footer(text=f'Attempted by: {ctx.message.author}', icon_url=ctx.author.avatar_url)
        await ctx.channel.send(embed=embed)
    else:
        if source == 'v11':
            source = f'https://raw.githubusercontent.com/discordjs/discord.js/docs/${source}.json'

        res = requests.get(DJS_DOCS_URI, params={ 'src': source, 'q': query }).json()
        embed = discord.Embed().from_dict(res)
        await ctx.channel.send(embed=embed)
